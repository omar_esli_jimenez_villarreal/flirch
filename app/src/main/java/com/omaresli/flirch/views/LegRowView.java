package com.omaresli.flirch.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.omaresli.flirch.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LegRowView extends FrameLayout {

    @Bind(R.id.text_view_segment_departure)
    TextView segmentDepartureText;

    @Bind(R.id.text_view_segment_arrival)
    TextView segmentArrivalText;

    @Bind(R.id.text_view_departure_time)
    TextView departureTimeText;

    @Bind(R.id.text_view_segment_duration_time)
    TextView segmentDurationTimeText;

    @Bind(R.id.text_view_airlines)
    TextView airlinesText;

    public LegRowView(Context context) {
        super(context);
        init(context);
    }

    public LegRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_leg_row, this);
        ButterKnife.bind(this);
    }

    public void setAirlinesText(String airlinesText) {
        this.airlinesText.setText(airlinesText);
    }

    public void setSegmentDurationTimeText(String segmentDurationTimeText) {
        this.segmentDurationTimeText.setText(segmentDurationTimeText);
    }

    public void setDepartureTimeText(String departureTimeText) {
        this.departureTimeText.setText(departureTimeText);
    }

    public void setSegmentDepartureText(String segmentDepartureText) {
        this.segmentDepartureText.setText(segmentDepartureText);
    }

    public void setSegmentArrivalText(String segmentArrivalText) {
        this.segmentArrivalText.setText(segmentArrivalText);
    }
}
