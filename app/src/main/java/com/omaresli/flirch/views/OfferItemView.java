package com.omaresli.flirch.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.omaresli.flirch.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OfferItemView extends FrameLayout {
    @Bind(R.id.text_view_outbound_segment_departure)
    TextView outboundSegmentDepartureText;

    @Bind(R.id.text_view_outbound_segment_arrival)
    TextView outboundSegmentArrivalText;

    @Bind(R.id.text_view_outbound_departure_time)
    TextView outboundDepartureTimeText;

    @Bind(R.id.text_view_outbound_segment_duration_time)
    TextView outboundSegmentDurationTimeText;

    @Bind(R.id.text_view_outbound_airlines)
    TextView outboundAirlinesText;

    @Bind(R.id.text_view_return_segment_departure)
    TextView returnSegmentDepartureText;

    @Bind(R.id.text_view_return_segment_arrival)
    TextView returnSegmentArrivalText;

    @Bind(R.id.text_view_return_departure_time)
    TextView returnDepartureTimeText;

    @Bind(R.id.text_view_return_segment_duration_time)
    TextView returnSegmentDurationTimeText;

    @Bind(R.id.text_view_return_airlines)
    TextView returnAirlinesText;

    @Bind(R.id.text_view_total_price)
    TextView totalPriceText;

    public OfferItemView(Context context) {
        super(context);
        init();
    }

    public OfferItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_offer_item, this);
        ButterKnife.bind(this);
    }

    public void setTotalPriceText(String totalPriceText) {
        this.totalPriceText.setText(totalPriceText);
    }

    public void setReturnAirlinesText(String returnAirlinesText) {
        this.returnAirlinesText.setText(returnAirlinesText);
    }

    public void setReturnSegmentDurationTimeText(String returnSegmentDurationTimeText) {
        this.returnSegmentDurationTimeText.setText(returnSegmentDurationTimeText);
    }

    public void setReturnDepartureTimeText(String returnDepartureTimeText) {
        this.returnDepartureTimeText.setText(returnDepartureTimeText);
    }

    public void setReturnSegmentDepartureText(String returnSegmentDepartureText) {
        this.returnSegmentDepartureText.setText(returnSegmentDepartureText);
    }

    public void setReturnSegmentArrivalText(String returnSegmentArrivalText) {
        this.returnSegmentArrivalText.setText(returnSegmentArrivalText);
    }

    public void setOutboundAirlinesText(String outboundAirlinesText) {
        this.outboundAirlinesText.setText(outboundAirlinesText);
    }

    public void setOutboundSegmentDurationTimeText(String outboundSegmentDurationTimeText) {
        this.outboundSegmentDurationTimeText.setText(outboundSegmentDurationTimeText);
    }

    public void setOutboundDepartureTimeText(String outboundDepartureTimeText) {
        this.outboundDepartureTimeText.setText(outboundDepartureTimeText);
    }

    public void setOutboundSegmentDepartureText(String outboundSegmentDepartureText) {
        this.outboundSegmentDepartureText.setText(outboundSegmentDepartureText);
    }

    public void setOutboundSegmentArrivalText(String outboundSegmentArrivalText) {
        this.outboundSegmentArrivalText.setText(outboundSegmentArrivalText);
    }
}
