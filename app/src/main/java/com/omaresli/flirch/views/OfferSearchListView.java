package com.omaresli.flirch.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.omaresli.flirch.R;
import com.omaresli.flirch.adapter.OfferItemAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OfferSearchListView extends FrameLayout {

    @Bind(R.id.recycler_view_offer_list)
    RecyclerView offerListRecyclerView;

    @Bind(R.id.progress_bar_offer_list)
    ProgressBar offerListProgressBar;

    @Bind(R.id.text_view_search_origin_label)
    TextView originText;

    @Bind(R.id.text_view_search_destination_label)
    TextView destinationText;

    @Bind(R.id.text_view_search_ticket_count)
    TextView ticketCountText;

    @Bind(R.id.text_view_departure_time)
    TextView departureTimeText;

    @Bind(R.id.text_view_return_time)
    TextView returnTimeText;

    public OfferSearchListView(Context context) {
        super(context);
        init();
    }

    public OfferSearchListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_offer_search_list, this);
        ButterKnife.bind(this);

        offerListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void setLoading(boolean isLoading) {
        offerListProgressBar.setVisibility(isLoading ? VISIBLE : GONE);
    }

    public void setOfferAdapter(OfferItemAdapter adapter) {
        offerListRecyclerView.setAdapter(adapter);
    }

    public void setReturnTimeText(String returnTimeText) {
        this.returnTimeText.setText(returnTimeText);
    }

    public void setDepartureTimeText(String departureTimeText) {
        this.departureTimeText.setText(departureTimeText);
    }

    public void setTicketCountText(String ticketCountText) {
        this.ticketCountText.setText(ticketCountText);
    }

    public void setDestinationText(String destinationText) {
        this.destinationText.setText(destinationText);
    }

    public void setOriginText(String originText) {
        this.originText.setText(originText);
    }
}
