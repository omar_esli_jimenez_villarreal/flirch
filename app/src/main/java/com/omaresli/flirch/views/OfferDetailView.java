package com.omaresli.flirch.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.omaresli.flirch.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OfferDetailView extends FrameLayout {

    @Bind(R.id.layout_offer_departure_flight_container)
    LinearLayout flightContainerLayout;

    @Bind(R.id.text_view_offer_departure_title)
    TextView offerDepartureTitleText;

    @Bind(R.id.text_view_offer_departure_ticket_class)
    TextView offerDepartureTicketClassText;

    @Bind(R.id.layout_offer_return_flight_container)
    LinearLayout flightContainerReturnLayout;

    @Bind(R.id.text_view_offer_return_title)
    TextView offerReturnTitleText;

    @Bind(R.id.text_view_offer_return_ticket_class)
    TextView offerReturnTicketClassText;

    @Bind(R.id.text_view_price)
    TextView priceText;

    public OfferDetailView(Context context) {
        super(context);
        init(context);
    }

    public OfferDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_offer_detail, this);
        ButterKnife.bind(this);
    }

    public void setOfferDepartureTicketClassText(String offerDepartureTicketClassText) {
        this.offerDepartureTicketClassText.setText(offerDepartureTicketClassText);
    }

    public void setOfferDepartureTitleText(String offerDepartureTitleText) {
        this.offerDepartureTitleText.setText(offerDepartureTitleText);
    }

    public void addDepartureSegmentView(LegRowView segmentRowView) {
        this.flightContainerLayout.addView(segmentRowView);
    }

    public void setOfferReturnTitleText(String offerReturnTitleText) {
        this.offerReturnTitleText.setText(offerReturnTitleText);
    }

    public void setOfferReturnTicketClassText(String offerReturnTicketClassText) {
        this.offerReturnTicketClassText.setText(offerReturnTicketClassText);
    }

    public void addReturnSegmentView(LegRowView segmentRowView) {
        this.flightContainerReturnLayout.addView(segmentRowView);
    }

    public void setPriceText(String priceText) {
        this.priceText.setText(priceText);
    }
}
