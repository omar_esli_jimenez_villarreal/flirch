package com.omaresli.flirch.network;

public class SearchOffersBody {
    public String origin;
    public String destination;
    public long departureDate;
    public long returnDate;
    public int ticketCount;
}
