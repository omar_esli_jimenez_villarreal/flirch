package com.omaresli.flirch.network;

import com.omaresli.flirch.model.Airline;
import com.omaresli.flirch.model.Airport;
import com.omaresli.flirch.model.Flight;
import com.omaresli.flirch.model.Leg;
import com.omaresli.flirch.model.Offer;
import com.omaresli.flirch.model.Segment;
import com.omaresli.flirch.model.Supplier;
import com.omaresli.flirch.model.TicketClass;

import java.util.HashMap;
import java.util.Map;

public class SearchOfferResult {
    public String originName;
    public String originCode;
    public String destinationName;
    public String destinationCode;

    public Airline[] airlines;
    public transient Map<Integer, Airline> airlineMap;

    public Airport[] airports;
    public transient Map<Integer, Airport> airportMap;

    public TicketClass[] ticketClasses;
    public transient Map<Integer, TicketClass> ticketClassMap;

    public Flight[] flights;
    public transient Map<Integer, Flight> flightMap;

    public Leg[] legs;
    public transient Map<Integer, Leg> legMap;

    public Segment[] segments;
    public transient Map<Integer, Segment> segmentMap;

    public Supplier[] suppliers;
    public transient Map<Integer, Supplier> supplierMap;

    public Offer[] offers;
}
