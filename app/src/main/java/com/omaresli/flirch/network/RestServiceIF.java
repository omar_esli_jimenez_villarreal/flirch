package com.omaresli.flirch.network;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RestServiceIF {
    String BASE_URL = "http://momondo-interview.herokuapp.com/";

    @POST("search")
    Call<SearchOfferResult> searchOffers(@Body SearchOffersBody searchBody);

    class Builder {
        public static RestServiceIF build() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(RestServiceIF.class);
        }
    }
}
