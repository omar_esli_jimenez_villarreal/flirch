package com.omaresli.flirch.network;

public class RestService {
    public static RestServiceIF restService = null;

    public static RestServiceIF get() {
        if (RestService.restService == null) {
            RestService.restService = RestServiceIF.Builder.build();
        }

        return RestService.restService;
    }
}
