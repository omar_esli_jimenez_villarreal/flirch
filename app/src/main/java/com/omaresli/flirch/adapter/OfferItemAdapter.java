package com.omaresli.flirch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.omaresli.flirch.model.Offer;
import com.omaresli.flirch.viewmodel.OfferItemViewModel;
import com.omaresli.flirch.views.OfferItemView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OfferItemAdapter extends RecyclerView.Adapter<OfferItemAdapter.ViewHolder> {

    private List<OfferItemViewModel> offerItemViewModels;

    public OfferItemAdapter() {
        this.offerItemViewModels = new ArrayList<>();
    }

    public void setData(Offer[] offers, OfferItemViewModel.Listener listener) {
        for (Offer offer : offers) {
            this.offerItemViewModels.add(new OfferItemViewModel(offer, listener));
        }
        Collections.sort(offerItemViewModels);
        notifyDataSetChanged();
    }

    @Override
    public OfferItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        OfferItemView view = new OfferItemView(parent.getContext());

        return new OfferItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OfferItemAdapter.ViewHolder holder, int position) {
        this.offerItemViewModels.get(position).bind(holder.offerItemView);
    }

    @Override
    public int getItemCount() {
        return offerItemViewModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final OfferItemView offerItemView;

        public ViewHolder(OfferItemView offerItemView) {
            super(offerItemView);
            this.offerItemView = offerItemView;
        }
    }
}
