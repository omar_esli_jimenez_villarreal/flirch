package com.omaresli.flirch.model;

import org.parceler.Parcel;

@Parcel
public class Airport {
    public int id;
    public String name;
}
