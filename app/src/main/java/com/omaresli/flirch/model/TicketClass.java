package com.omaresli.flirch.model;

import org.parceler.Parcel;

@Parcel
public class TicketClass {
    public int id;
    public String name;
}
