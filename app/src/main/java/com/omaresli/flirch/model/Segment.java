package com.omaresli.flirch.model;

import org.parceler.Parcel;

@Parcel
public class Segment {
    public int id;
    public long duration;
    public int[] legIndices;

    public Leg[] legs;
}
