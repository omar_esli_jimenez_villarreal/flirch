package com.omaresli.flirch.model;

import org.parceler.Parcel;

@Parcel
public class Offer {
    public int id;
    public double price;
    public int flightIndex;
    public int ticketClassIndex;

    public Flight flight;
    public TicketClass ticketClass;
}
