package com.omaresli.flirch.model;

import org.parceler.Parcel;

@Parcel
public class Airline {
    public int id;
    public String name;
}
