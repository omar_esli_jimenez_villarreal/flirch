package com.omaresli.flirch.model;

import org.parceler.Parcel;

@Parcel
public class Leg {
    public int id;
    public int airlineIndex;
    public int flightNumber;
    public long departure;
    public long arrival;
    public int destinationIndex;
    public int originIndex;

    public Airline airline;
    public Flight flight;
    public Airport destinationAirport;
    public Airport originAirport;
}
