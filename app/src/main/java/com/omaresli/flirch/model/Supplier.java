package com.omaresli.flirch.model;

public class Supplier {
    public int id;
    public String name;
    public int[] offerIndices;
}
