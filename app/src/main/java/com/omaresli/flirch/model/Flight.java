package com.omaresli.flirch.model;

import org.parceler.Parcel;

@Parcel
public class Flight {
    public int id;
    public int ticketClassIndex;
    public int[] segmentIndices;

    public TicketClass ticketClass;
    public Segment[] segments;
}
