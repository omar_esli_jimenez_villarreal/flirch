package com.omaresli.flirch.viewmodel;

public abstract class ViewModel<View> {
    public abstract void bind(View view);
}