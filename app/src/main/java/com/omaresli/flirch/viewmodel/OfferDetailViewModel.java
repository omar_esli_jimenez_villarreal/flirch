package com.omaresli.flirch.viewmodel;

import android.content.Context;
import android.text.format.DateUtils;

import com.omaresli.flirch.R;
import com.omaresli.flirch.model.Leg;
import com.omaresli.flirch.model.Offer;
import com.omaresli.flirch.utils.MoneyHelper;
import com.omaresli.flirch.views.LegRowView;
import com.omaresli.flirch.views.OfferDetailView;

import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;

public class OfferDetailViewModel extends ViewModel<OfferDetailView> {

    private Offer offer;

    public OfferDetailViewModel(Offer offer) {
        this.offer = offer;
    }

    @Override
    public void bind(OfferDetailView offerDetailView) {
        offerDetailView.setPriceText(offerDetailView.getContext().getString(
                R.string.offer_detail_price, MoneyHelper.formatAmount(offer.price, "DKK")));//We have no currency in result

        offerDetailView.setOfferDepartureTitleText(
                offerDetailView.getContext().getString(R.string.offer_departure_title,
                        DateUtils.formatDateTime(offerDetailView.getContext(),
                                offer.flight.segments[0].legs[0].departure,
                                DateUtils.FORMAT_SHOW_DATE)));

        offerDetailView.setOfferDepartureTicketClassText(offer.ticketClass.name);

        for (Leg leg : offer.flight.segments[0].legs) {
            LegRowView segmentRowView = createLegRowView(offerDetailView.getContext(), leg);
            offerDetailView.addDepartureSegmentView(segmentRowView);
        }

        offerDetailView.setOfferReturnTitleText(
                offerDetailView.getContext().getString(R.string.offer_return_title,
                        DateUtils.formatDateTime(offerDetailView.getContext(),
                                offer.flight.segments[1].legs[0].departure,
                                DateUtils.FORMAT_SHOW_DATE)));

        offerDetailView.setOfferReturnTicketClassText(offer.ticketClass.name);

        for (Leg leg : offer.flight.segments[1].legs) {
            LegRowView segmentRowView = createLegRowView(offerDetailView.getContext(), leg);
            offerDetailView.addReturnSegmentView(segmentRowView);
        }
    }

    private LegRowView createLegRowView(Context context, Leg leg) {
        LegRowView segmentRowView = new LegRowView(context);

        segmentRowView.setAirlinesText(leg.airline.name);

        StringBuilder sbReturn = new StringBuilder();
        Formatter returnFormatter = new Formatter(sbReturn, Locale.getDefault());
        segmentRowView.setDepartureTimeText(DateUtils.formatDateRange(
                context,
                returnFormatter,
                leg.departure,
                leg.arrival,
                DateUtils.FORMAT_SHOW_TIME,
                TimeZone.getDefault().getDisplayName()).toString());

        segmentRowView.setSegmentDurationTimeText(
                DateUtils.formatElapsedTime((leg.arrival - leg.departure) / 1000));

        segmentRowView.setSegmentDepartureText(leg.originAirport.name);

        segmentRowView.setSegmentArrivalText(leg.destinationAirport.name);

        return segmentRowView;
    }
}
