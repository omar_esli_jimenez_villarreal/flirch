package com.omaresli.flirch.viewmodel;

import android.content.Context;
import android.text.format.DateUtils;
import android.widget.Toast;

import com.omaresli.flirch.R;
import com.omaresli.flirch.adapter.OfferItemAdapter;
import com.omaresli.flirch.model.Airline;
import com.omaresli.flirch.model.Airport;
import com.omaresli.flirch.model.Flight;
import com.omaresli.flirch.model.Leg;
import com.omaresli.flirch.model.Offer;
import com.omaresli.flirch.model.Segment;
import com.omaresli.flirch.model.Supplier;
import com.omaresli.flirch.model.TicketClass;
import com.omaresli.flirch.network.RestService;
import com.omaresli.flirch.network.SearchOfferResult;
import com.omaresli.flirch.network.SearchOffersBody;
import com.omaresli.flirch.views.OfferSearchListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchOfferViewModel extends ViewModel<OfferSearchListView> {
    private OfferItemViewModel.Listener offerItemListener;
    private OfferItemAdapter offerAdapter;
    private OfferSearchListView offerSearchListView;

    public SearchOfferViewModel(OfferItemViewModel.Listener listener) {
        this.offerItemListener = listener;
    }

    @Override
    public void bind(OfferSearchListView offerSearchListView) {
        this.offerAdapter = new OfferItemAdapter();
        this.offerSearchListView = offerSearchListView;
        offerSearchListView.setOfferAdapter(offerAdapter);
    }

    public void searchFlightOffers(final SearchOffersBody searchBody) {
        if (offerSearchListView != null) {
            offerSearchListView.setLoading(true);

            Context context = offerSearchListView.getContext();
            offerSearchListView.setOriginText(context.getString(R.string.search_origin_label, searchBody.origin));
            offerSearchListView.setDestinationText(context.getString(R.string.search_destination_label, searchBody.destination));
            offerSearchListView.setTicketCountText(context.getString(R.string.search_ticket_count, searchBody.ticketCount));
            offerSearchListView.setDepartureTimeText(context.getString(R.string.search_departure_time,
                    DateUtils.formatDateTime(context, searchBody.departureDate, DateUtils.FORMAT_ABBREV_ALL)));
            offerSearchListView.setReturnTimeText(context.getString(R.string.search_return_time,
                    DateUtils.formatDateTime(context, searchBody.returnDate, DateUtils.FORMAT_ABBREV_ALL)));

            Call<SearchOfferResult> call = RestService.get().searchOffers(searchBody);
            call.enqueue(new Callback<SearchOfferResult>() {
                @Override
                public void onResponse(Call<SearchOfferResult> call, Response<SearchOfferResult> response) {
                    if (response.isSuccessful()) {
                        SearchOfferResult searchOfferResult = response.body();
                        if (searchOfferResult.offers == null || searchOfferResult.offers.length == 0) {
                            Toast.makeText(offerSearchListView.getContext(),
                                    offerSearchListView.getContext().getString(R.string.error_empty_offer), Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                // Convert array objects into Map for O(1) look up
                                searchOfferResult.airlineMap = new HashMap<>(searchOfferResult.airlines.length);
                                for (Airline airline : searchOfferResult.airlines) {
                                    searchOfferResult.airlineMap.put(airline.id, airline);
                                }

                                searchOfferResult.airportMap = new HashMap<>(searchOfferResult.airports.length);
                                for (Airport airport : searchOfferResult.airports) {
                                    searchOfferResult.airportMap.put(airport.id, airport);
                                }

                                searchOfferResult.ticketClassMap = new HashMap<>(searchOfferResult.ticketClasses.length);
                                for (TicketClass ticketClass : searchOfferResult.ticketClasses) {
                                    searchOfferResult.ticketClassMap.put(ticketClass.id, ticketClass);
                                }

                                searchOfferResult.flightMap = new HashMap<>(searchOfferResult.flights.length);
                                for (Flight flight : searchOfferResult.flights) {
                                    searchOfferResult.flightMap.put(flight.id, flight);
                                }

                                searchOfferResult.legMap = new HashMap<>(searchOfferResult.legs.length);
                                for (Leg leg : searchOfferResult.legs) {
                                    searchOfferResult.legMap.put(leg.id, leg);
                                }

                                searchOfferResult.segmentMap = new HashMap<>(searchOfferResult.segments.length);
                                for (Segment segment : searchOfferResult.segments) {
                                    searchOfferResult.segmentMap.put(segment.id, segment);
                                }

                                searchOfferResult.supplierMap = new HashMap<>(searchOfferResult.suppliers.length);
                                for (Supplier supplier : searchOfferResult.suppliers) {
                                    searchOfferResult.supplierMap.put(supplier.id, supplier);
                                }

                                for (Offer offer : searchOfferResult.offers) {

                                    offer.flight = searchOfferResult.flightMap.get(offer.flightIndex);
                                    offer.ticketClass = searchOfferResult.ticketClassMap.get(offer.ticketClassIndex);

                                    offer.flight.ticketClass = searchOfferResult.ticketClassMap.get(offer.flight.ticketClassIndex);
                                    List<Segment> segments = new ArrayList<>(offer.flight.segmentIndices.length);
                                    for (int index = 0; index < offer.flight.segmentIndices.length; index++) {
                                        segments.add(searchOfferResult.segmentMap.get(offer.flight.segmentIndices[index]));
                                    }
                                    offer.flight.segments = segments.toArray(new Segment[segments.size()]);

                                    for (Segment segment : offer.flight.segments) {
                                        List<Leg> legs = new ArrayList<>(segment.legIndices.length);
                                        for (int index = 0; index < segment.legIndices.length; index++) {
                                            legs.add(searchOfferResult.legMap.get(segment.legIndices[index]));
                                        }
                                        segment.legs = legs.toArray(new Leg[legs.size()]);

                                        for (Leg leg : legs) {
                                            leg.airline = searchOfferResult.airlineMap.get(leg.airlineIndex);
                                            leg.destinationAirport = searchOfferResult.airportMap.get(leg.destinationIndex);
                                            leg.originAirport = searchOfferResult.airportMap.get(leg.originIndex);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                offerSearchListView.setLoading(false);
                                return;
                            }
                            offerAdapter.setData(searchOfferResult.offers, offerItemListener);
                            offerSearchListView.setLoading(false);
                        }
                    } else {
                        Toast.makeText(offerSearchListView.getContext(),
                                offerSearchListView.getContext().getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SearchOfferResult> call, Throwable t) {
                    offerSearchListView.setLoading(false);
                    Toast.makeText(offerSearchListView.getContext(),
                            offerSearchListView.getContext().getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
