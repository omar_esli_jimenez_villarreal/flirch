package com.omaresli.flirch.viewmodel;

import android.text.format.DateUtils;
import android.view.View;

import com.omaresli.flirch.model.Leg;
import com.omaresli.flirch.model.Offer;
import com.omaresli.flirch.model.Segment;
import com.omaresli.flirch.utils.MoneyHelper;
import com.omaresli.flirch.views.OfferItemView;

import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;

public class OfferItemViewModel extends ViewModel<OfferItemView> implements Comparable {

    public interface Listener {
        void onOfferItemClicked(Offer offer);
    }

    private Offer offer;
    private Listener listener;

    public OfferItemViewModel(Offer offer, Listener listener) {
        this.offer = offer;
        this.listener = listener;
    }

    @Override
    public void bind(OfferItemView offerItemView) {

        if (listener != null)
            offerItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onOfferItemClicked(offer);
                }
            });

        offerItemView.setTotalPriceText(MoneyHelper.formatAmount(offer.price, "DKK"));//We have no currency in result

        String returnAirlines = "";
        String separator = "";
        Segment returnSegment = offer.flight.segments[1];
        Leg[] returnLegs = returnSegment.legs;
        for (Leg leg : returnLegs) {
            returnAirlines = returnAirlines + separator + leg.airline.name;
            separator = ",";
        }
        offerItemView.setReturnAirlinesText(returnAirlines);

        offerItemView.setReturnSegmentDurationTimeText(
                DateUtils.formatElapsedTime(returnSegment.duration));

        StringBuilder sbReturn = new StringBuilder();
        Formatter returnFormatter = new Formatter(sbReturn, Locale.getDefault());
        offerItemView.setReturnDepartureTimeText(
                DateUtils.formatDateRange(offerItemView.getContext(),
                        returnFormatter,
                        returnLegs[0].departure,
                        returnLegs[returnLegs.length - 1].arrival,
                        DateUtils.FORMAT_SHOW_TIME,
                        TimeZone.getDefault().getDisplayName()).toString());

        offerItemView.setReturnSegmentDepartureText(returnLegs[0].originAirport.name);
        offerItemView.setReturnSegmentArrivalText(returnLegs[returnLegs.length - 1].destinationAirport.name);

        String outboundAirlines = "";
        separator = "";
        Segment outboundSegment = offer.flight.segments[0];
        Leg[] outboundLegs = outboundSegment.legs;
        for (Leg leg : outboundLegs) {
            outboundAirlines = outboundAirlines + separator + leg.airline.name;
            separator = ",";
        }
        offerItemView.setOutboundAirlinesText(outboundAirlines);

        offerItemView.setOutboundSegmentDurationTimeText(
                DateUtils.formatElapsedTime(outboundSegment.duration));

        StringBuilder sbOutbound = new StringBuilder();
        Formatter outboundFormatter = new Formatter(sbOutbound, Locale.getDefault());
        offerItemView.setOutboundDepartureTimeText(
                DateUtils.formatDateRange(offerItemView.getContext(),
                        outboundFormatter,
                        outboundLegs[0].departure,
                        outboundLegs[outboundLegs.length - 1].arrival,
                        DateUtils.FORMAT_SHOW_TIME,
                        TimeZone.getDefault().getDisplayName()).toString());

        offerItemView.setOutboundSegmentDepartureText(outboundLegs[0].originAirport.name);
        offerItemView.setOutboundSegmentArrivalText(outboundLegs[outboundLegs.length - 1].destinationAirport.name);
    }

    @Override
    public int compareTo(Object another) {
        if (another instanceof OfferItemViewModel) {
            if (((OfferItemViewModel) another).offer.price > offer.price) {
                return -1;
            }
            if (((OfferItemViewModel) another).offer.price == offer.price) {
                return 0;
            }

            return 1;
        }

        return 0;
    }
}
