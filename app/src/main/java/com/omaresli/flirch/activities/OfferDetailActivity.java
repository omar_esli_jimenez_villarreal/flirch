package com.omaresli.flirch.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.omaresli.flirch.R;
import com.omaresli.flirch.model.Offer;
import com.omaresli.flirch.viewmodel.OfferDetailViewModel;
import com.omaresli.flirch.views.OfferDetailView;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OfferDetailActivity extends AppCompatActivity {

    @Bind(R.id.offer_detail_view)
    OfferDetailView offerDetailView;

    private static final String OFFER_EXTRA_INTENT_NAME = "OFFER_EXTRA_INTENT_NAME";

    public static Intent getIntent(Context context, Offer offer) {
        Intent intent = new Intent(context, OfferDetailActivity.class);
        intent.putExtra(OfferDetailActivity.OFFER_EXTRA_INTENT_NAME, Parcels.wrap(offer));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_offer_detail);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Offer offer = Parcels.unwrap(intent.getParcelableExtra(OFFER_EXTRA_INTENT_NAME));

        if (offer == null) {
            finish();//We have nothing to show
            return;
        }

        OfferDetailViewModel offerDetailViewModel = new OfferDetailViewModel(offer);
        offerDetailViewModel.bind(offerDetailView);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.offer_detail_activity_title, offer.flight.id));
        }

    }
}
