package com.omaresli.flirch.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.omaresli.flirch.R;
import com.omaresli.flirch.model.Offer;
import com.omaresli.flirch.network.SearchOffersBody;
import com.omaresli.flirch.viewmodel.OfferItemViewModel;
import com.omaresli.flirch.viewmodel.SearchOfferViewModel;
import com.omaresli.flirch.views.OfferSearchListView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OfferItemViewModel.Listener {

    @Bind(R.id.offer_search_list_view)
    OfferSearchListView offerSearchListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SearchOfferViewModel searchOfferViewModel = new SearchOfferViewModel(this);
        searchOfferViewModel.bind(offerSearchListView);

        SearchOffersBody searchOffersBody = new SearchOffersBody();
        searchOffersBody.origin = "CPH";
        searchOffersBody.destination = "PAR";
        searchOffersBody.departureDate = System.currentTimeMillis();
        searchOffersBody.returnDate = System.currentTimeMillis() + (10 * 24 * 60 * 60 * 1000);
        searchOffersBody.ticketCount = 1;
        searchOfferViewModel.searchFlightOffers(searchOffersBody);
    }

    @Override
    public void onOfferItemClicked(Offer offer) {
        startActivity(OfferDetailActivity.getIntent(getApplicationContext(), offer));
    }
}
