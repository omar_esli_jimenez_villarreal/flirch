package com.omaresli.flirch.utils;

public class MoneyHelper {

    public static String formatAmount(double amount, String currency) {
        return currency + " " + String.valueOf(amount);
    }
}
